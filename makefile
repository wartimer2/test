CC = g++
SOURCES = main.cpp
TARGET = app

all: $(TARGET)

$(TARGET): $(SOURCES)
	$(CC) $(SOURCES) -o $(TARGET)
